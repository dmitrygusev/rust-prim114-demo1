extern crate rand;
use rand::Rng;


fn main() {
    let n = 10000000;

    let mut v: Vec<i32> = Vec::new();//vec![];

    let mut rng = rand::thread_rng();

    let mut sum:i32 = 0;

    for i in 1..n {
        let x:i32 = rng.gen_range(-10,10);
        v.push(x);
        sum += x;
        //print!("{} ", x);
    }

    println!("= {}",sum);

    static NTHREADS: i32 = 10;


}
